(* ::Package:: *)

BeginPackage["u1MPSGroundStateSearch`"]


xxzHamiltonian::usage="xxzHamiltonian[n_, delta_:1] gives the XXZ-Hamiltonian for a spin chain of n spin-1/2 particles with OBC. The factor delta may break symmetries."


xxzMPO::usage="xxzMPO[n_, delta_:1] gives an MPO of the Hamiltonian in the XXZ model for a chain of n spin-1/2 particles with OBC."


tSVD::usage="tSVD[matr_, maxDim_] provides a thin SVD of any standard matrix matr given to it if maxDim < 0. The SVD is truncated, if its dimensions would exceed maxDim."


randomU1Tensor::usage="randomU1Tensor[range_, structure_] yields a random U1-tensor. The structure specified must in each entry contain information whether the described index is incoming or outgoing and what the degeneracies (values) for the possible spin quantum numbers (keys) are in an association."


xxzMPOu1::usage="xxzMPOu1[particleNo_,\[CapitalDelta]_] creates MPO of the XXZ-Hamiltonian using U(1)-tensors."


idMPOu1::usage="idMPOu1[particleNo_] creates MPO of the identity operator using U(1)-tensors."


reversePerm::usage="Receives a permutation as a list and returns the inverse permutation."


flipIndexU1::usage="flipIndexU1[tensor_,indexNo_] flips the indices at the positions in the list indexNo_ in their direction."


permuteU1::usage="permuteU1[tensor_, order_] receives a U1-tensor and an order of indices as a list and permutes the indices accordingly."


contractU1::usage="contractU1[t1_,t2_] receives two U1-tensors and contracts the last index of t1 i with the first index of t2 j. i and j have to have the same size and degeneracies and one has to be incoming, the other one outgoing."


fuseU1::usage="fuseU1[tensor_,fuseList_,newSign_] receives a tensor, a list of the indices to be fused (must be the last indices) and the sign of the fused index"


degTensorSplit::usage="degTensorSplit[degTensor_,degs_,fixedDegs_] finds the new degTensors in the split"


degSplitLoop::usage="degSplitLoop[degTensor_,degs_,fixedDegs_,indexCount_] iterates degTensorsplit sufficiently often for the degTensors in the split to be correct."


splitU1::usage="splitU1[tensor_,newIndices_] splits the last index of tensor_ into newIndices_."


matrixElement::usage="matrixElement[mps1_,mps2_,mpo_] calculates the matrix element of the states mps1_ & mps2_ and the operator mpo_."


traceU1Matrix::usage="traceU1Matrix[tensor_] gives the trace of tensor_ with the first two indices of tensor_ being dummy."


conjMPS::usage="conjMPS[mps_] creates and returns the complex conjugate mps to the given mps."


fuseAssocSpinHalf::usage="fuseAssocSpinHalf[assoc_,cap_] creates the combination of two indices in the U(1)-formalism with one index running over -1/2, 1/2 with a degeneracy of 1 each."


buildAssociationsSpinHalf::usage="buildAssociationsSpinHalf[particleNo_,cap_:-1] build a list that contains the indices for the U(1)-MPS of a spin chain of particleNo_ spin-1/2 particles."


svdU1::usage="svdU1[tensor_] performs a thin SVD on a U(1)-matrix."


normalizePos::usage="normalizePos[tensor_,normalizationDir_] normalizes the rank-three tensor given to the routine. normalizationDir=1 for right-normalization, normalizationDir=-1 for left-normalization."


rightNormalize::usage="rightNormalize[state_] right-normalizes an MPS from the last through the second tensor to bring the ansatz vector of the ground state search into a canonized form."


zipNet::usage="zipNet[mps1_,mps2_,mpo_,start_:1,end_:Length[mps1_]] builds the contracted tensor blocks by contracting the MPS, the MPO and the MPS of the adjoint vector up to or from a certain position, leaving the outer indices unfused."


buildHEff::usage="buildHEff[mps1_,mps2_,mpo_,chainpos_] constructs the matrix H^(eff) for the eigenvalue problem in the ground state search."


sweepStep::usage="sweepStep[mps1_,mps2_,chainpos_,indicesAtPos_,mpoHamiltonian_,normalizeDirection_:1] performs an optimization step at the position chainpos of a sweep in a certain direction, including the normalization of the optimized tensor."


sweep::usage="sweep[state_,stateConj_,indices_,mpoHamiltonian_,direction_] utilizes the routine 'sweepstep' to perform a sweep of optimization over the whole MPS."


minimizeEnergy::usage="minimizeEnergy[particleNo_,\[CapitalDelta]_:1,cap_:-1,epsilon_:10^(-10),rndRange_:{1}] approximates the ground state of the XXZ spin-1/2 chain for particleNo sites. Anisotropy is determined by \[CapitalDelta], the maximum degree of degeneracy in each spin quantum number by cap (cap=-1 means no truncating) and the threshold for the variance by epsilon. Returns the MPS for the approximated ground state in a list with the approximated ground state energy."


Begin["Private`"]


xxzHamiltonian[n_, delta_:1]:= Module[{sx, sy, sz, id, krx, kry, krz, i},
	{sx, sy, sz, id} = {{{0,1},{1,0}},{{0,-I},{I,0}},{{1,0},{0,-1}},{{1,0},{0,1}}};
	{krx, kry, krz} = KroneckerProduct[1/2#, 1/2#] &/@{sx, sy, sz};
	\!\(
\*SubsuperscriptBox[\(\[Sum]\), \(i = 1\), \(n - 1\)]\((Nest[KroneckerProduct[#, \ id] &, \ Nest[KroneckerProduct[id, \ #] &, \ krx, \ i - 1], \ n - i - 1]\[IndentingNewLine]\t\t + \ Nest[KroneckerProduct[#, \ id] &, \ Nest[KroneckerProduct[id, \ #] &, \ kry, \ i - 1], \ n - i - 1]\[IndentingNewLine]\t\t + \ Nest[KroneckerProduct[#, \ id] &, \ Nest[KroneckerProduct[id, \ #] &, \ delta*krz, \ i - 1], \ n - i - 1])\)\)
]


(*Creates the XXZ MPO for particleNo_ particles and the anisotropy \[CapitalDelta] without utilizing the U(1)-structure.*)
xxzMPO[particleNo_, \[CapitalDelta]_:1]:= Module[{sx, sy, sz, id, o, start, middle, end, mpo},
	{sx, sy, sz, id} = 1/2*{{{0,1},{1,0}},{{0,-I},{I,0}},{{1,0},{0,-1}},{{1,0},{0,1}}};
	id = 2*id;
	o = {{0,0},{0,0}};
	start = {{o, sx, sy, \[CapitalDelta]*sz, id}};
	middle = {
		{id, o, o, o, o},
		{sx, o, o, o, o},
		{sy, o, o, o, o},
		{sz, o, o, o, o},
		{o, sx, sy, \[CapitalDelta]*sz, id}
	};
	end = Flatten[{{id, sx, sy, sz, o}}, {{2}, {1}, {3}, {4}}];
	mpo = {start,Sequence@@Table[middle,particleNo-2],end}
]


(*Conducts a thin SVD on a standard matrix with the possibility of truncation if a size of the matrix exceeds maxDim.*)
tSVD[matr_, maxDim_:-1]:=If[Min[Dimensions[matr]] > maxDim && maxDim > 0, 
	SingularValueDecomposition[matr, maxDim], 
	SingularValueDecomposition[matr, Min[Dimensions[matr]]]]


(*Routine given to me by Michael Weyrauch to construct a U(1)-tensor with the spin quantum numbers and degeneracies given in the list 'structure' with random entries where the entries are a priori not zero.*)
randomU1Tensor[range_, structure_]:= Module[{arrayStruct, signs, tab, assoc}, arrayStruct = Transpose[{Length[#[[2]]], {First[#], Last[#]} &@Keys[#[[2]]]} &/@structure];
	signs = First/@structure;
	assoc = structure[[All, 2]];
	{Array[
		If[Plus@@({##}*signs)==0,
		tab=Part@@@Transpose[{assoc,Key/@{##}}];
		RandomReal[range,tab],
		0]&,Sequence@@arrayStruct],
		structure
	}
]


(*Creates the U(1)-symmetric XXZ-MPO for particleNo_ spin-1/2 particles using the anisotropy \[CapitalDelta].*)
xxzMPOu1[particleNo_,\[CapitalDelta]_]:=Module[
{startHu1,midHu1,endHu1},
	startHu1={{{{{0,{{{{1/Sqrt[2]}}}}},{0,0}},{{{{{{0}},{{\[CapitalDelta]/2}},{{1}}}},0},{0,{{{{0}},{{-\[CapitalDelta]/2}},{{1}}}}}},{{0,0},{{{{{1/Sqrt[2]}}}},0}}}},{{-1,<|0->1|>},{1,<|-1->1,0->3,1->1|>},{-1,<|-(1/2)->1,1/2->1|>},{1,<|-(1/2)->1,1/2->1|>}}};
	midHu1={{{{{{{{{0}}}},0},{0,{{{{0}}}}}},{{0,0},{{{{{1/Sqrt[2]}},{{0}},{{0}}}},0}},{{0,0},{0,0}}},{{{0,{{{{0}}},{{{0}}},{{{1/Sqrt[2]}}}}},{0,0}},{{{{{{1}},{{0}},{{0}}},{{{1/2}},{{0}},{{0}}},{{{0}},{{\[CapitalDelta]/2}},{{1}}}},0},{0,{{{{1}},{{0}},{{0}}},{{{-1/2}},{{0}},{{0}}},{{{0}},{{-\[CapitalDelta]/2}},{{1}}}}}},{{0,0},{{{{{0}}},{{{0}}},{{{1/Sqrt[2]}}}},0}}},{{{0,0},{0,0}},{{0,{{{{1/Sqrt[2]}},{{0}},{{0}}}}},{0,0}},{{{{{{0}}}},0},{0,{{{{0}}}}}}}},{{-1,<|-1->1,0->3,1->1|>},{1,<|-1->1,0->3,1->1|>},{-1,<|-(1/2)->1,1/2->1|>},{1,<|-(1/2)->1,1/2->1|>}}};
	endHu1={{{{{0,0},{{{{{1/Sqrt[2]}}}},0}}},{{{{{{{1}}},{{{1/2}}},{{{0}}}},0},{0,{{{{1}}},{{{-1/2}}},{{{0}}}}}}},{{{0,{{{{1/Sqrt[2]}}}}},{0,0}}}},{{-1,<|-1->1,0->3,1->1|>},{1,<|0->1|>},{-1,<|-(1/2)->1,1/2->1|>},{1,<|-(1/2)->1,1/2->1|>}}};
	{startHu1,Sequence@@Table[midHu1,particleNo-2],endHu1}
]


(*Creates the U(1)-symmetric identity-MPO for particleNo_ spin-1/2 particles.*)
idMPOu1[particleNo_]:=Table[{{{{{{{{{1}}}}},{0}},{{0},{{{{{1}}}}}}}},{{1,<|0->1|>},{-1,<|-(1/2)->1,1/2->1|>},{1,<|-(1/2)->1,1/2->1|>},{-1,<|0->1|>}}},particleNo]


(*reversePerm gives the inverse of a permutation list perm_ in form of a list.*)
reversePerm[perm_]:=Module[{check, temp},
	check=perm-Range[Length[perm]];
	temp=Range[Length[perm]];
	Do[
		If[check[[i]]!=0,
			temp=ReplacePart[temp,i+check[[i]]->i];
		],{i,Length[perm]}
	];
	temp
]


(*Flips the indices at the positions given in the list indexNo_ of the U(1)-tensor tensor_.*)
flipIndexU1[tensor_,indexNo_]:=Module[{signs,assocs},

	{signs,assocs}=Transpose[tensor[[2]]];
	signs[[indexNo]]=-1*signs[[indexNo]];

	{
		Reverse[tensor[[1]],indexNo],
		Transpose[{signs,assocs}]
	}
]


(*Permutes indices in U(1)-tensors tensor_ where the new order of indices is given in a list order_.*)
permuteU1[tensor_, order_]:=Module[{oldStruct,newStruct,arrayStruct,oldOffsets,signs},

	oldStruct=tensor[[2]];
	newStruct=tensor[[2,##]]&@order;
	arrayStruct=Transpose[{Length[#[[2]]], {First[#], Last[#]} &@Keys[#[[2]]]} &/@newStruct];
	oldOffsets=First/@(Keys[#[[2]]]&/@oldStruct);
	signs=First/@newStruct;

	{Array[
		If[Plus@@({##}*signs)==0 && (tensor[[1,Sequence@@({##}[[reversePerm[order]]]-oldOffsets+1)]])=!=0,(*second condition is to avoid "empty" degTensors, e.g. zeros, to get an exception in Flatten*)
			Flatten[
			tensor[[1,Sequence@@({##}[[reversePerm[order]]]-oldOffsets+1)]],{#}&/@order],
			0]&,Sequence@@arrayStruct],
	newStruct
	}
]


(*Contract two U(1)-tensors over the last index of the first tensor t1 and the first index of the second tensor t2, which have to be identical except for their direction, which has to be opposite.*)
contractU1[t1_,t2_]:=Module[{struct,contrRange,arrayStruct,signs,t1Rank,offsets,contrOffset,oldPos,contrInd,compT1,compT2,assoc},

	(*Builds the structure of the resulting tensor.*)
	t1Rank=Length[t1[[2]]];
	struct=Flatten[{t1[[2,1;;t1Rank-1]],t2[[2,2;;Length[t2[[2]]]]]},1];
	arrayStruct = Transpose[{Length[#[[2]]], {First[#], Last[#]} &@Keys[#[[2]]]} &/@struct];

	(*Gets information about locations of degeneracy tensors in t1 and t2 as well as directions of indices.*)
	contrOffset=First[Keys[t1[[2,t1Rank,2]]]];
	offsets = First/@(Keys[#[[2]]]&/@struct);
	contrRange = Last[Keys[#[[2]]]&/@t1[[2]]];
	signs = {First/@struct, Last[Transpose[t1[[2]]][[1]]]};
	assoc = struct[[All,2]];

	(*Build the irregular array and the object the tensor is saved in.*)
	{Array[
		If[Plus@@({##}*signs[[1]])==0,
			oldPos={##}-offsets+1;
			contrInd=-(Plus@@(({##}*signs[[1]])[[;;t1Rank-1]]))*signs[[2]];
			(*Check another condition to see if a new degeneracy tensor filled with zeros needs to be created that was not present in the old tensors.*)
			If[Abs[contrInd]<=Abs[Keys[Last[t1[[2]]][[2]]][[1]]],
				(*Find the degeneracy tensors compT1 and compT2 that need to be multiplied and contract them.*)
				compT1=t1[[1,Sequence@@(oldPos[[;;t1Rank-1]]),contrInd-contrOffset+1]];
				compT2=t2[[1,contrInd-contrOffset+1,Sequence@@(oldPos[[t1Rank;;]])]];
				Dot[compT1,compT2],
				(*Create new degeneracy tensor filled with zeros. The zeros were implicitly written by the placeholder zeros in the old tensors t1 and t2.*)
				Array[0&,Part@@@Transpose[{assoc,Key/@{##}}]]],
			0]&,Sequence@@arrayStruct],
		struct
	}
]


fuseU1[tensor_,fuseList_,newSign_]:=Module[
{oldOffsets,signs,assocs,keys,combinations,allCombinations,newSpins,fusedAssoc,fusedStruct,fusedSigns,arrayStruct,flattenOrder,spinTemp,spinCombs,strucPos,degParts,newOffsets,strucPosEnd},

	oldOffsets=First/@(Keys[#[[2]]]&/@tensor[[2]]);
	signs=First/@tensor[[2]];

	(*Compute the possible combinations of spins with their degeneracies.*)
	assocs=tensor[[2,fuseList,2]];
	keys=Keys[assocs];
	combinations=Tuples[keys];
	allCombinations=Tuples[Table[Sequence@@Table[#,assocs[[i]][#]]&/@(keys[[i]]),{i,Length[fuseList]}]];
	newSpins=Plus@@@allCombinations;

	(*Build structure of the resulting tensor*)
	fusedAssoc=AssociationThread[Union[newSpins]->(Count[newSpins,#]&/@Union[newSpins])];
	fusedStruct={Sequence@@tensor[[2,;;First[fuseList]-1]],{newSign,fusedAssoc},Sequence@@tensor[[2,Last[fuseList]+1;;]]};
	fusedSigns=First/@fusedStruct;
	arrayStruct=Transpose[{Length[#[[2]]], {First[#], Last[#]} &@Keys[#[[2]]]} &/@fusedStruct];

	newOffsets=First/@(Keys[#[[2]]]&/@fusedStruct);

	(*Information about which degeneracy tensors need to be flattened together later.*)
	flattenOrder={Sequence@@Table[{i},{i,First[fuseList]-1}],fuseList,Sequence@@Table[{i},{i,Last[fuseList]+1,Length[tensor[[2]]]}]}; 

	(*Actual composition of the U(1)-tensor resulting from the fusion.*)
	{
		Array[
			If[
				Plus@@({##}*fusedSigns)==0,
				spinTemp={##}[[First[fuseList]]];
				spinCombs=Select[combinations,(Plus@@(#*signs[[First[fuseList];;Last[fuseList]]]))==spinTemp*newSign&];
				strucPos={1,Sequence@@({##}[[;;First[fuseList]-1]]-oldOffsets[[;;First[fuseList]-1]]+1)};

				strucPosEnd={Sequence@@({##}[[First[fuseList]+1;;]]-newOffsets[[First[fuseList]+1;;]]+1)};

				(*Extracts the degeneracy tensors of the old tensor at positions with the same resulting spin and then puts them together to form a new degeneracy tensor.*)
				degParts=Flatten[tensor[[Sequence@@strucPos,Sequence@@(#-oldOffsets[[First[fuseList];;Last[fuseList]]]+1),Sequence@@strucPosEnd]],flattenOrder]&/@spinCombs;
				Join[Sequence@@degParts,First[fuseList]],
			0]&,Sequence@@arrayStruct],
		fusedStruct
	}
]


(*Help function to partition and reshape a tensor.*)
partitionSubtensor[subtensor_,coarse_,fine_,degs_]:=Module[{coarseSplit,fineSplit},

	coarseSplit=FoldPairList[TakeDrop,subtensor,coarse];
	fineSplit=MapThread[FoldPairList[TakeDrop,#1,#2]&,{coarseSplit,fine}];
	MapThread[ArrayReshape[#1,#2]&,Flatten[#,1]&/@{fineSplit,degs}]
]


(*Gets the degeneracy tensor of a U(1)-tensor to be split and returns the set of degeneracy tensors this old degeneracy tensor is split into.*)
degTensorSplit[degTensor_,degs_,fixedDegs_]:=Module[
{degTensorFitted,coarseStruct,fineStruct,result},

	fineStruct=Apply[Times,degs,{2}];
	coarseStruct=Plus@@@fineStruct;
	degTensorFitted={degTensor};
	result=Flatten[Array[
		partitionSubtensor[degTensorFitted[[##]],coarseStruct,fineStruct,degs]
		&,fixedDegs
		],1];
	If[
		Length[fixedDegs]==1,
		result,
		Flatten[result,{{2},{1},{3}}]
	]
]


splitU1[tensor_,newIndices_]:=Module[
{oldIndices,oldSigns,newSigns,oldOffsets,newOffsets,resultingOffset,newSpinCombs,newKeys,keyStruct,resultingIndices,newStruct,newIndicesStart,calcCheck,pos,structPos,splitIndexKey,fittingNewIndices,extractionPairs,degeneracies,fixedDegs,args,degTensors,calcCheckCount},

	(*Create dummy index to handle the splitting of U(1)-vectors.*)
	oldIndices={{1,<|0->1|>},Sequence@@(tensor[[2]])};

	oldSigns=First[#]&/@oldIndices;
	newSigns=First[#]&/@newIndices;

	oldOffsets=First/@(Keys[#[[2]]]&/@oldIndices);
	newOffsets=First/@(Keys[#[[2]]]&/@newIndices);
	resultingOffset={Sequence@@oldOffsets[[;;-2]],Sequence@@newOffsets};

	newSpinCombs=Tuples[(Keys[#[[2]]]&/@newIndices)];
	newKeys=Keys[#[[2]]]&/@newIndices;
	keyStruct=Transpose[{Length[#[[2]]], {First[#], Last[#]} &@Keys[#[[2]]]} &/@newIndices];
	resultingIndices={Sequence@@oldIndices[[;;-2]],Sequence@@newIndices};

	newStruct=Transpose[{Length[#[[2]]], {First[#], Last[#]} &@Keys[#[[2]]]} &/@resultingIndices];
	newIndicesStart=Length[oldIndices[[;;-2]]]+1;

	(*calcCheck serves as a check later, whether a group of degeneracy tensors belonging to a spin quantum number in the old tensor has already been prepared.*)
	calcCheck="calcCheck";

	(*Actual composition of the split U(1)-tensor.*)
	{
		Flatten[
			Array[
				If[
					(*Check the condition that the quantum numbers add up to zero.*)
					pos={##}*Flatten[{oldSigns[[;;-2]],newSigns},1];
					Plus@@pos==0,

					structPos={1,Sequence@@(Flatten[{pos[[2;;-(Length[newIndices]+1)]]*oldSigns[[2;;-2]],Plus@@(pos[[newIndicesStart;;]]*Last[oldSigns])},1]-oldOffsets[[2;;]]+1)};

					splitIndexKey=(Plus@@(pos[[newIndicesStart;;]]));

					(*Checks whether the degeneracy tensor for this spin combination in the old tensor has already been treated. If not, said degeneracy tensor is split into a set of degeneracy tensors. With the iteration of the split spin quantum numbers, this set is the distributed to the correct places in the new tensor.*)
					If[calcCheck!=({##}[[newIndicesStart-1]]),

						(*Compute combinations of quantum numbers that add up to the old quantum number in question.*)
						fittingNewIndices=Table[Select[newSpinCombs,(#[[1]]==i&&Plus@@(#*newSigns)==splitIndexKey)&],{i,First[newKeys[[1]]],Last[newKeys[[1]]]}];
						fittingNewIndices=Select[fittingNewIndices,UnsameQ[#,{}]&];

						extractionPairs=(Flatten[{Table[Transpose[newIndices][[2]],Length[#]],#},{{2},{3},{1}}])&/@fittingNewIndices;

						(*Gather the arguments for degTensorSplit and let said routine create the set of new degeneracy tensors.*)
						degeneracies=Map[#[[1]][#[[2]]]&,extractionPairs,{3}];
						fixedDegs=oldIndices[[#,2]][pos[[;;Length[oldIndices]-1]][[#]]]&/@Range[Length[oldIndices]-1];
						args={tensor[[Sequence@@structPos]],degeneracies,fixedDegs};
						degTensors=degTensorSplit[Sequence@@args];

						calcCheck=({##}[[newIndicesStart-1]]);
						calcCheckCount=1,
						calcCheckCount=calcCheckCount+1;
					];
					degTensors[[calcCheckCount]],
					0
				]&,Sequence@@newStruct],1],
		resultingIndices[[2;;]]
	}
]


(*Evaluates the matrix element of the operator corresponding to mpo with the states belonging to mps1 and mps2.*)
matrixElement[mps1_,mps2_,mpo_]:=Module[
{prepMPS1,prepMPS2,prepMPO,transferMartrices,transferMartricesFused,zippedBlock},

	(*Permutes the indices in preparation for the construction of the transfer matrices.*)
	prepMPS1=permuteU1[#,{1,3,2}]&/@(mps1);
	prepMPS2=permuteU1[#,{2,1,3}]&/@(mps2);
	prepMPO=permuteU1[#,{3,1,4,2}]&/@(mpo);

	(*Actual contraction of the tensors to get the matrix element.*)
	transferMartrices=permuteU1[contractU1[contractU1[prepMPS1[[#]],prepMPO[[#]]],prepMPS2[[#]]],{2,4,6,1,3,5}]&/@Range[Length[prepMPS1]];
	transferMartricesFused=fuseU1[permuteU1[fuseU1[#,{4,5,6},1],{4,1,2,3}],{2,3,4},-1]&/@transferMartrices;
	zippedBlock=Fold[contractU1,transferMartricesFused];
	traceU1Matrix[zippedBlock]
]


(*Expects a U(1)-tensor with the first to indices being dummy and with OBC effectively just removes the dummy indices.*)
traceU1Matrix[tensor_]:=Module[{struct,newstruct,matrixSize,arrayStruct,signs,offsets},

	struct=tensor[[2]];

	If[Length[struct]>2,
		newstruct=struct[[3;;Length[struct]]];
		matrixSize=Length[struct[[3,2]]];
		arrayStruct=Transpose[{Length[#[[2]]], {First[#], Last[#]} &@Keys[#[[2]]]} &/@newstruct];
		signs=First/@newstruct;
		offsets=First/@(Keys[#[[2]]]&/@newstruct);
		{
			Array[
				If[Plus@@({##}*signs)==0,
					tensor[[1,1,1,Sequence@@({##}-offsets+1),1,1]],
					0]&,Sequence@@arrayStruct
			],
			newstruct
		},	
		tensor[[1,1,1,1,1]]
	]
]


(*Expects one MPS tensor with three legs, the middle one being the physical index, and returns the corresponding MPS tensor of the adjoint state.*)
conjMPS[mps_]:=Module[{sign},
	{Reverse[Reverse[mps[[1]]],3],ReplacePart[mps[[2]],{2,1}->-mps[[2,2,1]]]}
]


(*Builds the combination of two indices where one runs over -1/2, 1/2 with both degeneracies being 1.*)
fuseAssocSpinHalf[assoc_,cap_]:=Module[
{halfLength,firstHalfKeys,firstHalfVals,keys,vals},
	halfLength=Length[assoc]/2;
	firstHalfKeys=Keys[assoc][[;;Floor[halfLength]+1]]-1/2;
	firstHalfVals={1,Sequence@@Table[assoc[firstHalfKeys[[i]]-1/2]+assoc[firstHalfKeys[[i]]+1/2],{i,2,Length[firstHalfKeys]}]};
	{keys,vals}={Sequence@@#,Sequence@@(Abs[Reverse[#[[;;Ceiling[halfLength]]]]])}&/@{firstHalfKeys,firstHalfVals};
	vals=If[cap>-1&&#>cap,cap,#]&/@vals;
	AssociationThread[keys,vals]
]


(*Builds the indices for a spin-1/2 chain of length particleNo_. Optionally, the degeneracies in the indices can be capped by cap_.*)
buildAssociationsSpinHalf[particleNo_,cap_:-1]:=Module[{physSpin,physKeys,physAssoc,assocsHalf,firstAssoc, assocs},
	physSpin=1/2;
	physKeys=Range[-physSpin,physSpin,1];
	physAssoc=AssociationThread[physKeys,1];
	firstAssoc=<|0->1|>;
	assocsHalf=Table[
		{
			firstAssoc,
			physAssoc,
			firstAssoc=fuseAssocSpinHalf[firstAssoc,cap]
		},{i,1,particleNo/2}];
	assocs={Sequence@@assocsHalf,Sequence@@(Reverse[assocsHalf,{1,2}])};
	Transpose[{{1,-1,-1},#}]&/@assocs
]


(*Performs a thin SVD on a matrix in its U(1)-symmetric form. The SVD can just be applied to the non-zero blocks individually.*)
svdU1[tensor_]:=Module[
{indices,minIndex,oldOffsets,smallestIndexVal,blockmatrices,oldStructure,decompositions,arrayStructs,structures},

	(*Set up array-structures for the decomposed matrices U, S and V^t^*.*)
	oldStructure=tensor[[2]];
	oldOffsets=First/@(Keys[#[[2]]]&/@oldStructure);
	indices=oldStructure[[All,2]];
	minIndex=SortBy[indices,Length[#]&][[1]];
	structures={{{1,indices[[1]]},{-1,minIndex}},
				{{1,minIndex},{-1,minIndex}},
				{{-1,indices[[2]]},{1,minIndex}}};
	arrayStructs=Table[Transpose[{Length[#[[2]]], {First[#], Last[#]} &@Keys[#[[2]]]} &/@(structures[[i]])],{i,3}];

	(*Extract the blockmatrices and decompose them via thin SVD.*)
	smallestIndexVal=Sort[oldOffsets][[-1]];
	blockmatrices=Table[tensor[[1,Sequence@@({i,i}-oldOffsets+1)]],{i,smallestIndexVal,-smallestIndexVal}];
	decompositions=Transpose[tSVD/@blockmatrices];

	(*Arrange the matrices acquired by the decomposition into a U(1)-tensor structure.*)
	Table[
		{
			Array[
				If[
					Subtract@@{##}==0,
					decompositions[[i,({##}-smallestIndexVal+1)[[1]]]],
					0
				]
				&,Sequence@@arrayStructs[[i]]],
			structures[[i]]
		},{i,3}]
]


(*Normalizes a tensor given to the routine with regard to a given direction, 1 for right-normalization, -1 for left-normalization. The routine then multiplies the residue of the decomposition as one matrix and returns the normalized tensor, as well as the residue.*)
normalizePos[tensor_,normalizationDir_]:=Module[
{svdTarget,svdResult,splitTarget,svdResidue,normalizedTensor},

	If[
		normalizationDir==-1,

		svdTarget=fuseU1[tensor,{1,2},1];
		svdResult=svdU1[svdTarget];
		{splitTarget,svdResidue}={svdResult[[1]],svdResult[[2;;]]};
		svdResidue={svdResidue[[1]],permuteU1[svdResidue[[2]],{2,1}]};
		normalizedTensor=permuteU1[splitU1[permuteU1[splitTarget,{2,1}],tensor[[2,1;;2]]],{2,3,1}],

		svdTarget=fuseU1[tensor,{2,3},-1];
		svdResult=svdU1[svdTarget];
		{splitTarget,svdResidue}={svdResult[[3]],svdResult[[;;2]]};
		splitTarget=permuteU1[splitTarget,{2,1}];
		normalizedTensor=splitU1[splitTarget,tensor[[2,2;;3]]]
	];
	{normalizedTensor,Fold[contractU1,svdResidue]}
]


(*Right-normalizes every tensor except for the first one in the initial guess starting from the last position and going towards the first.*)
rightNormalize[state_]:=Module[
{length,updState,targetTensor,svdFactor,normalizedTensor},

	length=Length[state];
	updState={};
	svdFactor={{{{{1}}}},{{1,<|0->1|>},{-1,<|0->1|>}}};
	Do[
		targetTensor=contractU1[state[[pos]],svdFactor];
		{normalizedTensor,svdFactor}=normalizePos[targetTensor,1];
		updState={normalizedTensor,Sequence@@updState};
		,{pos,length,2,-1}
	];
	updState={contractU1[state[[1]],svdFactor],Sequence@@updState}
]


(*Creates the left or right blocks used in building the matrix H^(eff), i.e. contracts and fuses an MPS with the MPO and the MPS of the adjoint vector up to or from a certain position.*)
zipNet[mps1_,mps2_,mpo_,start_:1,end_:Length[mps1_]]:=Module[
{dist,prepMPS1,prepMPS2,prepMPO,transferMatrices,middleFused={},first,last},
	dist=end-start;

	(*Permute the tensors in a way that the correct indices can be contracted*)
	prepMPS1=permuteU1[#,{1,3,2}]&/@(mps1[[start;;end]]);
	prepMPS2=permuteU1[#,{2,1,3}]&/@(mps2[[start;;end]]);
	prepMPO=permuteU1[#,{3,1,4,2}]&/@(mpo[[start;;end]]);

	transferMatrices=permuteU1[contractU1[contractU1[prepMPS1[[#]],prepMPO[[#]]],prepMPS2[[#]]],{2,4,6,1,3,5}]&/@Range[Length[prepMPS1]];

	If[dist==0,
		Return[permuteU1[transferMatrices[[1]],{4,5,6,1,2,3}]]
	];

	middleFused=fuseU1[permuteU1[fuseU1[#,{4,5,6},1],{4,1,2,3}],{2,3,4},-1]&/@(transferMatrices[[2;;dist]]);
	first=fuseU1[permuteU1[First[transferMatrices],{4,5,6,1,2,3}],{4,5,6},-1];
	last=permuteU1[fuseU1[Last[transferMatrices],{4,5,6},1],{4,1,2,3}];

	Fold[contractU1,{first,Sequence@@middleFused,last}]
]


(*builHEff constructs the effective Hamiltonian that is used in the eigenvalue problem of the optimization step. It could potentially also construct the N-matrix when given the identity-MPO as parameter mpo_.*)
buildHEff[mps1_,mps2_,mpo_,chainpos_]:=Module[
{length,bounds,hEff,blockL,blockR,block,protoHEff},
	length=Length[mps1];

	(*The condition handles treatment of the outermost positions, which is different from the general case of chainpos_ being somewhere in the middle of the tensor chain.*)
	If[(length==chainpos||1==chainpos),
		If[1==chainpos,

			bounds={2,length};
			(*Builds the effective Hamiltonian for the case that the position to optimize is the first in the MPS.*)
			block=flipIndexU1[permuteU1[fuseU1[zipNet[mps1,mps2,mpo,Sequence@@bounds],{4,5,6},-1],{2,1,3,4}],3];
			protoHEff=contractU1[mpo[[chainpos]],block];
			hEff=fuseU1[permuteU1[fuseU1[permuteU1[protoHEff,{6,1,3,4,2,5}],{5,6},-1],{1,2,5,3,4}],{4,5},1],

			bounds={1,chainpos-1};
			(*Builds the effective Hamiltonian for the case that the position to optimize is the last in the MPS.*)
			block=flipIndexU1[permuteU1[fuseU1[permuteU1[zipNet[mps1,mps2,mpo,Sequence@@bounds],{4,5,6,1,2,3}],{4,5,6},1],{4,1,3,2}],3];
			protoHEff=contractU1[block,mpo[[chainpos]]];
			hEff=fuseU1[permuteU1[fuseU1[permuteU1[protoHEff,{6,1,2,5,3,4}],{5,6},-1],{1,2,5,3,4}],{4,5},1]
		],
	(*Builds the effective Hamiltonian for the case that the position to optimize is somewhere in the middle of the MPS.*)
	blockL=flipIndexU1[permuteU1[fuseU1[permuteU1[zipNet[mps1,mps2,mpo,1,chainpos-1],{4,5,6,1,2,3}],{4,5,6},1],{4,1,3,2}],3];
	blockR=flipIndexU1[permuteU1[fuseU1[zipNet[mps1,mps2,mpo,chainpos+1,Length[mps1]],{4,5,6},-1],{2,1,3,4}],3];
	protoHEff=contractU1[contractU1[blockL,mpo[[chainpos]]],blockR];
	hEff=fuseU1[permuteU1[fuseU1[permuteU1[protoHEff,{1,8,5,2,6,4,3,7}],{6,7,8},-1],{1,2,6,3,4,5}],{4,5,6},1]
	];
	hEff
]


(*Solves the posed eigenvalue problem for the blockdiagonal U(1)-matrix H^(eff) for the lowest eigenvector in the spin-0 sector.*)
buildLowestEV[hEff_,identFactor_:1000]:=Module[
{zeroBlockESys,offsets,zeroPos,evIndex,evArrayStruct},

	offsets=First[Keys[hEff[[2,1,2]]]];
	evIndex={hEff[[2,1]]};
	evArrayStruct=Transpose[{Length[#[[2]]], {First[#], Last[#]} &@Keys[#[[2]]]} &/@evIndex];
	zeroPos=0-offsets+1;
	(*To get around the fact that the eigenvalues are sorted by their absolute values in Mathematica, the eigenvalues are first pushed into a positive range by an offset identFactor, which is subtracted after solving for the lowest eigenvalue.*)
	zeroBlockESys=Eigensystem[(hEff[[1,zeroPos,zeroPos]]+identFactor*IdentityMatrix[hEff[[2,1,2]][0]]),-1]-{identFactor,0};

	{zeroBlockESys[[1,1]],
		(*Build the U(1)-eigenvector.*)
		{Array[
			If[##==0,
				zeroBlockESys[[2,1]],
			0]&,Sequence@@evArrayStruct],evIndex}
	}
]


(*Computes an optimization step at the position chainpos_ and normalizes the updated tensor.*)
sweepStep[mps1_,mps2_,chainpos_,indicesAtPos_,mpoHamiltonian_,normalizeDirection_:1]:=Module[
{hEff,lowestEV,updatedTensor,svdResidue,erg},

	(*Build effective Hamiltonian and solve the eigenvalue problem.*)
	hEff=buildHEff[mps1,mps2,mpoHamiltonian,chainpos];
	{erg,lowestEV}=buildLowestEV[traceU1Matrix[hEff]];

	(*Split the eigenvector just found to gain the updated tensor at the current position and normalize it.*)
	updatedTensor=splitU1[lowestEV,indicesAtPos[[{2,1,3}]]];
	updatedTensor=permuteU1[updatedTensor,{2,1,3}];
	{updatedTensor,svdResidue}=normalizePos[updatedTensor,normalizeDirection];

	(*Multiply the residual matrix from the SVD to the left or right tensor (depending on the sweeping direction) and return the locally optimized MPS.*)
	If[normalizeDirection==1,
		{{Sequence@@(mps1[[;;chainpos-2]]),contractU1[mps1[[chainpos-1]],svdResidue],updatedTensor,Sequence@@(mps1[[chainpos+1;;]])},erg},
		{{Sequence@@(mps1[[;;chainpos-1]]),updatedTensor,contractU1[svdResidue,mps1[[chainpos+1]]],Sequence@@(mps1[[chainpos+2;;]])},erg}
	]
]


(*Puts sweepStep together to make a sweep. direction_ is 1 for left to right, -1 for right to left sweep.*)
sweep[state_,stateConj_,indices_,mpoHamiltonian_,direction_]:=Module[
{length,bounds,updState,updStateConj,erg},

	length=Length[state];
	bounds=Range[length];

	updState=state;
	updStateConj=stateConj;
	Do[
		{updState,erg}=sweepStep[updState,updStateConj,i,indices[[i]],mpoHamiltonian,-direction];
		updStateConj=conjMPS[#]&/@updState,
		{i,bounds[[direction]],bounds[[direction*(length-1)]],direction}];
	{updState,updStateConj,erg}
]


minimizeEnergy[particleNo_,\[CapitalDelta]_:1,cap_:-1,epsilon_:10^(-10),rndRange_:{1}]:=Module[
{indices,state,stateConj,mpoHamiltonian,prepMPO,mpoSqunfused,mpoSq,erg,var},

	(*Create a random U(1)-mps for the given amount of particles in the spin chain & right-normalize it.*)
	indices=buildAssociationsSpinHalf[particleNo,cap];
	state=rightNormalize[randomU1Tensor[rndRange,#]&/@indices];
	stateConj=conjMPS[#]&/@state;

	(*Build the MPOs that are passed down for the other functions.*)
	mpoHamiltonian=permuteU1[#,{1,3,4,2}]&/@xxzMPOu1[particleNo,\[CapitalDelta]];
	prepMPO=permuteU1[#,{2,1,4,3}]&/@mpoHamiltonian;
	mpoSqunfused=MapThread[contractU1[#1,#2]&,{prepMPO,prepMPO}];
	mpoSq=fuseU1[fuseU1[permuteU1[#,{2,4,1,6,3,5}],{1,2},-1],{4,5},1]&/@mpoSqunfused;

	erg=matrixElement[state,stateConj,mpoHamiltonian];
	var=Abs[(matrixElement[state,stateConj,mpoSq]-erg^2)];

	(*Loop that conducts the sweeps to the right, then to the left.*)
	While[var>epsilon,
		{state,stateConj,erg}=sweep[state,stateConj,indices,mpoHamiltonian,1];
		{state,stateConj,erg}=sweep[state,stateConj,indices,mpoHamiltonian,-1];
		var=Abs[(matrixElement[state,stateConj,mpoSq]-erg^2)];
	];
	{state,erg}
]


End[]


EndPackage[]
